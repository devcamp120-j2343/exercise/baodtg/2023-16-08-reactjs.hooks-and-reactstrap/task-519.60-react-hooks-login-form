import { Button, Col, Form, Row } from "reactstrap"

function SignUp({ signUpDisplayProps }) {
    const onBtnStartedClick = () => {
        console.log(document.getElementById('inp-firstname').value)
        console.log(document.getElementById('inp-lastname').value)
        console.log(document.getElementById('inp-email').value)
        console.log(document.getElementById('inp-password').value)
    }
    return (
        <>
            {signUpDisplayProps
                ? <Row>
                    <Form className="mt-4 mb-5"></Form>
                        <div className="text-center">
                            <h3 style={{ color: "white" }}>Sign Up For Free</h3>
                        </div>
                        <Row>
                            <Col md={6}>
                                <input className="w-100" id="inp-firstname" placeholder={"First Name*"} style={{ background: "none", border: "1px solid #91A3A1", color: "#91A3A1" }} />
                            </Col>
                            <Col md={6}>
                                <input className="w-100" id="inp-lastname" placeholder={"Last Name*"} style={{ background: "none", border: "1px solid #91A3A1", color: "#91A3A1" }} />
                            </Col>
                            <Col md={12} className="mt-3">
                                <input className="w-100" id="inp-email" placeholder={"Email Address*"} style={{ background: "none", border: "1px solid #91A3A1", color: "#91A3A1" }} />
                            </Col>
                            <Col md={12} className="mt-3">
                                <input className="w-100" id="inp-password" placeholder={"Set A Password*"} style={{ background: "none", border: "1px solid #91A3A1", color: "#91A3A1" }} />
                            </Col>
                            <Col className="mt-3 ">
                                <Button className="w-100" onClick={onBtnStartedClick} style={{ background: "#1AB188", fontSize: "25px" }}>GET STARTED</Button>
                            </Col>
                        </Row>

                    </Form>
                </Row>
                : <></>}


        </>
    )
}
export default SignUp