import { Button, Col, Container, Row } from "reactstrap"
import 'bootstrap/dist/css/bootstrap.min.css'
import SignUp from "./SignUpForm"
import LogIn from "./Login"
import { useState } from "react"

function LoginForm() {
    const [signupDisplay, setSignupDisplay] = useState(false);
    const [loginDisplay, setLoginDisplay] = useState(true);

    const [inputEmail, setInputEmail] = useState("");
    const [inputPassword, setInputPassword] = useState("");

    const inputEmailChangeHandler = (value) => {
        setInputEmail(value)

    }
    const inputPasswordChangeHandler = (value) => {
        setInputPassword(value)

    }

    const onBtnLoginClick = () => {
        setSignupDisplay(false);
        setLoginDisplay(true);

    }

    const onBtnSignUpClick = () => {
        setSignupDisplay(true);
        setLoginDisplay(false);

    }
    return (
        <>
            <Container style={{ width: "600px", background: "#24333C" }}>
                <Row >

                    <Col className="mt-5 text-center">
                        <Button onClick={onBtnSignUpClick} className="w-50">Sign Up</Button>
                        <Button onClick={onBtnLoginClick} className="w-50">Log In</Button>
                    </Col>
                </Row>
                <Row>
                    { }
                </Row>
                <SignUp signUpDisplayProps={signupDisplay} />
                <LogIn logInDisplayProps={loginDisplay} inputEmailChangeHandlerProps={inputEmailChangeHandler} inputPasswordChangeHandlerProps={inputPasswordChangeHandler} />



            </Container>
        </>
    )
}
export default LoginForm