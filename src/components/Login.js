import { Button, Col, Row } from "reactstrap"

function LogIn({ logInDisplayProps }) {

    const onBtnLogInClick = () => {
        if (!document.getElementById('inp-email').value.includes("@")) {
            alert("alo")
        }
        console.log(document.getElementById('inp-email').value)
        console.log(document.getElementById('inp-password').value)
    }
    return (
        <>
            {logInDisplayProps
                ? <div className="mt-4 mb-3">

                    <div className="text-center">
                        <h3 style={{ color: "white" }}>Welcome Back!</h3>
                    </div>
                    <Row>
                        <Col md={12} className="mt-3">
                            <input className="w-100" id="inp-email" type="email" placeholder="Email Address*" style={{ background: "none", border: "1px solid #91A3A1", color: "#91A3A1" }} />
                        </Col>
                        <Col md={12} className="mt-3">
                            <input className="w-100" type="password" id="inp-password" placeholder={"Set A Password*"} style={{ background: "none", border: "1px solid #91A3A1", color: "#91A3A1" }} />
                        </Col>
                        <Col className="mt-3 mb-5">
                            <Button onClick={onBtnLogInClick} type="button" className="w-100" style={{ background: "#1AB188", fontSize: "25px" }}>LOG IN</Button>
                        </Col>
                    </Row>

                </div>
                : <></>
            }


        </>
    )
}
export default LogIn